import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/MyServletDB")
public class MyServletDB extends HttpServlet {
   private static final long serialVersionUID = 1L;
   static String url = "jdbc:mysql://jaclar49:3306/techDB?enabledTLSProtocols=TLSv1.2";
   static String user = "jaclar49";
   static String password = "Jl#30583";
   static Connection connection = null;

   public MyServletDB() {
      super();
   }

   protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	   try {
		
		PrintWriter write = response.getWriter();
		write.println("<body>\r\n"
				+ "	\r\n"
				+ "	<form action=\"./MyServletDB\" method=\"post\">\r\n"
				+ "		<p>User ID (Any #):</p>\r\n"
				+ "		<input type=\"text\" name=\"id\"/>\r\n"
				+ "		<br/>\r\n"
				+ "		<p>First name:</p>\r\n"
				+ "		<input type=\"text\" name=\"firstName\"/>\r\n"
				+ "		<br/>\r\n"
				+ "		<p>Last name:</p>\r\n"
				+ "		<input type=\"text\" name=\"lastName\"/>\r\n"
				+ "		<br/>\r\n"
				+ "		<p>Time of reservation:</p>\r\n"
				+ "		<input type=\"text\" name=\"time\"/>\r\n"
				+ "		<br/>\r\n"
				+ "		<p>Phone Number:</p>\r\n"
				+ "		<input type=\"text\" name=\"phone\"/>\r\n"
				+ "		<br/>\r\n"
				+ "		<p>Number of people:</p>\r\n"
				+ "		<input type=\"text\" name=\"numPeople\"/>\r\n"
				+ "		<br/><br/><br/>\r\n"
				+ "		<input type=\"submit\"/>\r\n"
				+ "	</form>\r\n"
				+ "</body>");
		   
		Connection con = DatabaseConnection.initializeDatabase();
		PreparedStatement st = con.prepareStatement("insert into reservation_system values(?, ?, ?, ?, ?, ?)");
	
			
		
		
		
			st.setInt(1, Integer.valueOf(request.getParameter("id")));
			st.setString(2, request.getParameter("firstName"));
			st.setString(3, request.getParameter("lastName"));
			st.setString(4, request.getParameter("time"));
			st.setString(5, request.getParameter("phone"));
			st.setString(6, request.getParameter("numPeople"));
		
			st.executeUpdate();
		
			Statement statement = (Statement) con.createStatement();
			String sql;
			sql = "select * from reservation_system";
			ResultSet resultSet = statement.executeQuery(sql);
		
			write.println("<html><body><b>Reservation Successfully Made" + "</b</body></html>");
			write.println("</b><br><br>");
			write.println("If successful, see name below<br><br>");
		
			write.println("<b>Name:</b><br>");

			while (resultSet.next())
			{
				write.println(resultSet.getString("first"));
				write.println(resultSet.getString("last"));
				write.println("<br>");
			}
			st.close();
			con.close();
		
			
		
		
		
		
		
		
	} catch (ClassNotFoundException e) {
		e.printStackTrace();
	} catch (SQLException e) {
		e.printStackTrace();
	}
	   
   }

   protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      doGet(request, response);
   }
}